package com.example.demo.controllers;

import com.example.demo.db.DataRepository;
import com.example.demo.db.Identitification;
import com.example.demo.db.PersonalDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class Controller {
    @Autowired
    DataRepository dataRepository;

    @RequestMapping("/")
    public String hello() {

        UUID id = UUID.randomUUID();
        PersonalDetails paymentDetails = PersonalDetails.builder().id(id)
                                                        .property1("p1")
                                                        .property2("p2")
                                                        .property3("p3").build();

        System.out.println("paymentDetails ID = " + paymentDetails);
        List<Identitification> identitifications = Arrays.asList(
            new Identitification(paymentDetails.getId(), "key-111111", "value1"),
            new Identitification(paymentDetails.getId(), "key-222222", "value2"));

        paymentDetails.setIdentitifications(identitifications);

        PersonalDetails save = dataRepository.save(paymentDetails);

        return "Yes! Hello!";
    }


    @RequestMapping("/uuid/{uuid}")
    public PersonalDetails find(@PathVariable String uuid) {
        Optional<PersonalDetails> save = dataRepository.findById(UUID.fromString(uuid));

        return save.get();
    }
}
