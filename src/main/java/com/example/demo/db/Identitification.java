package com.example.demo.db;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Builder
@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class Identitification implements Serializable {
    public Identitification(UUID id, UUID personalDetailsId, String key, String pvalue) {
        this.id = id;
        this.personalDetailsId = personalDetailsId;
        this.pKey = key;
        this.pValue = pvalue;
    }

    public Identitification(UUID personalDetailsId, String key, String pvalue) {
        this.id = UUID.randomUUID();
        this.personalDetailsId = personalDetailsId;
        this.pKey = key;
        this.pValue = pvalue;
    }

    public Identitification() {
        this.id = UUID.randomUUID();
        personalDetailsId = null;
        pKey = null;
        pValue = null;
    }
//    @NotNull
//    @GeneratedValue
//    @EqualsAndHashCode.Include
//    @ToString.Include
//    @Type(type = "uuid-char")
//    @Column(columnDefinition = "char(36)")
//    final UUID id;

    @Id
    @NotNull
    @EqualsAndHashCode.Include
    @ToString.Include
    @Type(type = "uuid-char")
    @Column(columnDefinition = "char(36)")
    final UUID id;
    //    @Id
    @NotNull
    @EqualsAndHashCode.Include
    @ToString.Include
    @Type(type = "uuid-char")
    @Column(columnDefinition = "char(36)")
    final UUID personalDetailsId;

    //    @Id
    @EqualsAndHashCode.Include
    @ToString.Include
    @Column(columnDefinition = "char(128)")
    final String pKey;

    @EqualsAndHashCode.Include
    @ToString.Include
    final String pValue;
}
