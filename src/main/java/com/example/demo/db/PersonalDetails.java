package com.example.demo.db;

import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class PersonalDetails {
    @Id
    @Type(type = "uuid-char")
    @Column(columnDefinition = "char(36)")
    UUID id;

    @Singular("identity")
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "personalDetailsId")
    List<Identitification> identitifications;

    String property1;
    String property2;
    String property3;
    String property4;
}
