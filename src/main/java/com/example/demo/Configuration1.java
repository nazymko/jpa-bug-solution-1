package com.example.demo;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@EnableAutoConfiguration
@Configuration
@EnableJpaRepositories(basePackages = "com.example")
@EnableTransactionManagement
public class Configuration1 {
}
